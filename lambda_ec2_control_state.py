# Python 3
import boto3
import logging
from datetime import datetime

import json,ast

#setup simple logging for INFO
# logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger()
logger.setLevel(logging.INFO)

#define client connection
ec2client = boto3.client('ec2')

# This lambda might be call anytime. With a CloudWatch trigger. Every 5 minutes, MON-FRI. For example. e.g. cron(*/5 * ? * MON-FRI * )
# It will:
# * shutdown all EC2 instances which DO NOT have tag AutoStop:False based on:
#   * the value for tag 'stopTime' (format HHMM) or default to 'defaultStopTime' set below
# * start all EC2 instances which have the tag AutoStart:True based on:
#   * the value for tag 'startTime' (format HHMM) or default to 'defaultStartTime' set below
#   * AutoStop configuration as described just above to prevent looping over stop and start...
def lambda_handler(event, context):
    global ec2client
    global ec2resources

    # Get list of regions.
    # Leave the array empty to loop over all regions worldwide.
    # Restrict to only the regions you are using to optimize duration and cost
    regionsList = ec2client.describe_regions(RegionNames=['eu-west-1','eu-west-2','eu-west-3']).get('Regions',[] )

    # Get current time
    global now
    global defaultStartTime
    global defaultStopTime

    now = datetime.now()
    # Otherwise stated, start AutoStart machines at 9:00 (GMT+1)
    defaultStartTime=datetime.strptime('800','%H%M')
    # Otherwise stated, stop AutoStop machines at 19:00 (GMT+1)
    defaultStopTime=datetime.strptime('1800','%H%M')

    # Iterate over regions
    for region in regionsList:
        regionName=region['RegionName']
        logger.info("=================================")
        logger.info("Looking at region %s ", regionName)

        # Connect to region
        ec2resources = boto3.resource('ec2', region_name=regionName)

        # Stop running instances based on AutoStop and stopTime tags (default to defaultStopTime)
        stopEc2()

        # Start stopped instances based on AutoStop and stopTime tags (default to defaultStartTime)
        startEc2()
        
def stopEc2():
    # get a list of all instances
    filters = [
        {
            'Name': 'instance-state-name', 
            'Values': ['running']
        }
    ]

    all_running_instances = [i for i in ec2resources.instances.filter(Filters=filters)]
    logger.info("%d running instances", len(all_running_instances))
    if logger.isEnabledFor(logging.DEBUG):
        for instance in all_running_instances:
            logger.debug("Running instance: %s", instance.id)

    # get instances with filter of running + with tag `AutoStop:false`
    filters = [
        {
            'Name': 'instance-state-name', 
            'Values': ['stopped']
        },
        {
            'Name': 'tag:AutoStop', 
            'Values': ['False']
        }
    ]

    keep_alive_instances = [i for i in ec2resources.instances.filter(Filters=filters)]
    logger.info("%d instances to keep alive", len(keep_alive_instances))
    if logger.isEnabledFor(logging.DEBUG):
        for instance in keep_alive_instances:
            logger.debug("Instance to keep alive: %s", instance.id)

    # Filter from all instances the instance that are not designed to keep alive
    instances_to_stop = [to_stop for to_stop in all_running_instances if to_stop.id not in [i.id for i in keep_alive_instances]]
    logger.info("%d instances to potentially stop", len(instances_to_stop))

    for instance in instances_to_stop:
        logger.debug("Instance to stop? %s", instance.id)
        # Otherwise stated, stop AutoStop machines at defaultStopTime
        stopTime = None
        tagStopTimeSet = False
        # Check if a specific stop time has been defined for the instance
        for tag in instance.tags:
            if tag['Key'] == 'stopTime':
                stopTime = datetime.strptime(tag['Value'],'%H%M')
                tagStopTimeSet = True

        if not tagStopTimeSet:
            stopTime = defaultStopTime

        # Stop the instance if `now` if after defined stopTime (or default stopTime)
        if now.hour > stopTime.hour or (now.hour == stopTime.hour and now.minute >= stopTime.minute):
            instance.stop()
            if tagStopTimeSet:
                logger.info("Stopping %s because `now` (%s:%s) >= `tag:stopTime` (%s:%s)", instance.id, now.hour, now.minute, stopTime.hour,  stopTime.minute)
            else:
                logger.info("Stopping %s because `now` (%s:%s) >= `default stopTime` (%s:%s)", instance.id, now.hour, now.minute, stopTime.hour,  stopTime.minute)
        else:
            logger.info("Keep running %s because `now` (%s:%s) < `tag:stopTime` (%s:%s)", instance.id, now.hour, now.minute, stopTime.hour,  stopTime.minute)
            
def startEc2():
    # get a list of all instances
    filters = [
        {
            'Name': 'instance-state-name', 
            'Values': ['stopped']
        },
        {
            'Name': 'tag:AutoStart', 
            'Values': ['True']
        }
    ]

    instances_to_start = [i for i in ec2resources.instances.filter(Filters=filters)]
    logger.info("%d instances to potentially start", len(instances_to_start))
    for instance in instances_to_start:
        logger.debug("Instance to start? %s", instance.id)
        # Otherwise stated, start AutoStart machines at defaultStartTime
        startTime = None
        tagStartTimeSet = False
        # Otherwise stated, stop AutoStop machines at defaultStopTime
        stopTime = None
        tagStopTimeSet = False
        # Otherwise tagged, consider all instances are supposed to stop
        autoStop = True
        # Check if a specific start time has been defined for the instance
        for tag in instance.tags:
            if tag['Key'] == 'startTime':
                startTime = datetime.strptime(tag['Value'],'%H%M')
                tagStartTimeSet = True
            if tag['Key'] == 'stopTime':
                stopTime = datetime.strptime(tag['Value'],'%H%M')
                tagStopTimeSet = True
            if tag['Key'] == 'AutoStop':
                autoStop = tag['Value']

        if not tagStartTimeSet:
            startTime = defaultStartTime
        if not tagStopTimeSet:
            stopTime = defaultStopTime

        # Start the instance if `now` if after defined startTime (or default startTime) but before defined stopTime (or default stopTime)
        if now.hour > startTime.hour or (now.hour == startTime.hour and now.minute >= startTime.minute):
            # Before starting the instance, check that time is not over stopTime for all machines but the one explicitly tagged to NOT autostop --which must be restarted
            if autoStop == 'True' and (now.hour > stopTime.hour or (now.hour == stopTime.hour and now.minute >= stopTime.minute)):
                logger.info("Keeping stopped %s because `now` (%s:%s) >= `stopTime` (%s:%s)", instance.id, now.hour, now.minute, stopTime.hour,  stopTime.minute)
            else:
                instance.start()
                if autoStop == 'True':
                    if tagStartTimeSet:
                        logger.info("Starting %s because `tag:startTime` (%s:%s) <= `now` (%s:%s) < stopTime (%s:%s)", instance.id, startTime.hour, startTime.minute, now.hour, now.minute, stopTime.hour, stopTime.minute)
                    else:
                        logger.info("Starting %s because `default startTime` (%s:%s) <= `now` (%s:%s) < stopTime (%s:%s)", instance.id, startTime.hour, startTime.minute, now.hour, now.minute, stopTime.hour, stopTime.minute)
                else:
                    logger.info("Starting %s because `tag:startTime` (%s:%s) <= `now` (%s:%s)", instance.id, startTime.hour, startTime.minute, now.hour, now.minute)

        else:
            logger.info("Keeping stopped %s because `now` (%s:%s) < `tag:startTime` (%s:%s)", instance.id, now.hour, now.minute, startTime.hour, startTime.minute)