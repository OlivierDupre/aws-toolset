# Python 2.7
import boto3
import logging

#setup simple logging for INFO
logger = logging.getLogger()
logger.setLevel(logging.INFO)

#define the connection
ec2 = boto3.resource('ec2')

def lambda_handler(event, context):
    # Use the filter() method of the instances collection to retrieve all running EC2 instances that must be autostop.
    # TODO: Set here the filters needed to uniquely identify the instances that must be stopped.
    filters = [
        {
            'Name': 'instance-state-name', 
            'Values': ['running']
        },        
        {
            'Name': 'Name', 
            'Values': ['WHATEVER']
        },        
        {
            'Name': 'tag:project', 
            'Values': ['WHATEVER']
        },
        {
            'Name': 'tag:AutoStop',
            'Values': ['true']
        }
    ]
    
    #filter the instances
    instances = ec2.instances.filter(Filters=filters)

    #locate all running instances
    RunningInstances = [instance.id for instance in instances]
    
    #print the instances for logging purposes
    #print RunningInstances 
    
    #make sure there are actually instances to shut down. 
    if len(RunningInstances) > 0:
        #perform the shutdown
        shuttingDown = ec2.instances.filter(InstanceIds=RunningInstances).stop()
        print shuttingDown
    else:
        print "Nothing to see here"