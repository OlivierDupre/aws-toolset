# Python 3
import boto3
import logging
import json,ast

#setup simple logging for INFO
logger = logging.getLogger()
logger.setLevel(logging.INFO)

#define the connection
ec2 = boto3.resource('ec2')

# This lambda must be call with an input event to define the `goal`: 'start' or 'stop' the EC2, and fine tune the `filters` to find the EC2 to start or stop.
# e.g.
# {
#   "goal": "stop",
#   "filters": [
#     {
#       "Name": "tag:role",
#       "Values": ["nodes"]
#     },
#     {
#       "Name": "tag:project",
#       "Values": ["aforge"]
#     }
#   ]
# }
def lambda_handler(event, context):
    try:
        goal = ast.literal_eval(json.dumps(event['goal']))
    except:
        goal = "nothing"
    print("Goal: ", goal)
    
    try:
        event_filters = ast.literal_eval(json.dumps(event['filters']))
    except:
        print("No filters provided to this lambda")
    print("Filters: ", event_filters)
        
    if goal == 'start':
        start_ec2(event, context, event_filters)
    elif goal == 'stop':
        stop_ec2(event, context, event_filters)
    else:
        print("No goal provide to this lambda")

def start_ec2(event, context, event_filters):
    # Use the filter() method of the instances collection to retrieve all running EC2 instances.
    local_filters = [
        {
            'Name': 'instance-state-name', 
            'Values': ['stopped']
        },
        {
            'Name': 'tag:AutoStart',
            'Values': ['true']
        }
    ]
    
    filters = local_filters + event_filters
    
    print(("Start filters", filters))

    #filter the instances
    instances = ec2.instances.filter(Filters=filters)

    #locate all running instances
    filteredInstances = [instance.id for instance in instances]
    
    #print the instances for logging purposes
    #print RunningInstances 
    
    #make sure there are actually instances to shut down. 
    if len(filteredInstances) > 0:
        #perform the shutdown
        Starting = ec2.instances.filter(InstanceIds=filteredInstances).start()
        print(Starting)
    else:
        print("No instances to start using these filters. ", filters)

def stop_ec2(event, context, event_filters):
    # Use the filter() method of the instances collection to retrieve all running EC2 instances.
   # Use the filter() method of the instances collection to retrieve all running EC2 instances.
    # TODO: Set here the filters needed to uniquely identify the instances that must be stopped.
    local_filters = [
        {
            'Name': 'instance-state-name', 
            'Values': ['running']
        },
        {
            'Name': 'tag:AutoStop',
            'Values': ['true']
        }
    ]
    
    filters = local_filters + event_filters
    
    print(("Stop filters",filters))
    
    #filter the instances
    instances = ec2.instances.filter(Filters=filters)

    #locate all running instances
    filteredInstances = [instance.id for instance in instances]

    #make sure there are actually instances to shut down. 
    if len(filteredInstances) > 0:
        #perform the shutdown
        Stopping = ec2.instances.filter(InstanceIds=filteredInstances).stop()
        print(Stopping)
    else:
        print("No instances to stop using these filters. ", filters)