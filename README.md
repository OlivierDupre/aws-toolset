# aws-toolset
A set of tools for AWS.

## Lambda Stop & Start EC2
2 lambdas to start or stop EC2 based on an event.
This event can be a trigger from CloudWatch.

Instances must be tag with `AutoStart=true` or `AutoStop=true` for usage with corresponding lambdas.
Filters to uniquely identify the instances to start or stop are defined in the body of the lambda and must be adapted accordingly.

### Trigger events from CloudWatch
Once the lambda has been saved, go to:
* CloudWatch
  * Events
    * Rules
      * Create Rule
        * Schedule
        * Cron Expression: e.g. `0 18 ? * MON-FRI *`
        * Add targets:
          * Select the lambda